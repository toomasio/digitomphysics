using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomPhysics
{
    public class AimCast : MonoBehaviour
    {
        [SerializeField] protected Transform aimRoot = null;
        [SerializeField] protected SphereCastSettings settings = default;

        public SphereCast DetectCast { get; protected set; }

        private void Awake()
        {
            DetectCast = new SphereCast(aimRoot, settings);
        }

        private void FixedUpdate()
        {
            DetectCast.Tick();
        }
    }
}


