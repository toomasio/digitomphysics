﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomPhysics
{
    [System.Serializable]
    public class AddForce3DSettings
    {
        public Vector3 forceDirection;
        public float forcePower;
        public Space space;
        public ForceMode forceMode;

    }

    public class AddForce3D
    {
        private readonly Rigidbody rb;
        private Transform relativeTo;
        private readonly AddForce3DSettings settings;

        public AddForce3D(Rigidbody targetRb, Transform relativeTo, AddForce3DSettings settings)
        {
            this.rb = targetRb;
            this.settings = settings;
        }

        public void Tick()
        {
            var dir = settings.space == Space.Self ? relativeTo.TransformDirection(settings.forceDirection) : settings.forceDirection;
            rb.AddForce(dir * settings.forcePower, settings.forceMode);
        }
    }
}