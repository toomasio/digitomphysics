﻿using UnityEngine;

namespace DigitomPhysics
{
    public class InteractCanvas : MonoBehaviour
    {
        [SerializeField] private InteractableEvents interactableEvents = null;

        public void OnHoverEnter()
        {
            interactableEvents.onHoverEnter?.Invoke();
            Debug.Log($"entereed");
        }

        public void OnHoverExit()
        {
            interactableEvents.onHoverExit?.Invoke();
            Debug.Log($"exited");
        }

        public void OnInteract()
        {
            interactableEvents.onInteract?.Invoke();
        }
    }
}


