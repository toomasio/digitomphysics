﻿using UnityEngine;
using UnityEngine.InputSystem;
using DigitomUtilities;
using System;

namespace DigitomPhysics
{
    public class InteractSphereCast : DetectClosestComponent<SphereCastSettings, SphereCast, Interactable>
    {
        public InteractSphereCast(GameObject owner, Transform detectOrigin, SphereCastSettings data, bool closestHitOnly) : base(owner, detectOrigin, data, closestHitOnly) { }

        protected override Type[] IgnoreComponents => null;

        protected override void OnDetectEnter()
        {
        }

        protected override void OnDetectExit()
        {
        }
    }
}


