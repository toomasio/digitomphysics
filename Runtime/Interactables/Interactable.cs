﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace DigitomPhysics
{
    [System.Serializable]
    public class InteractableEvents
    {
        public UnityEvent onHoverEnter;
        public UnityEvent onHoverExit;
        public UnityEvent onInteract;
    }

    public class Interactable : MonoBehaviour , IInteractable, IDetectable
    {
        [SerializeField] protected InteractableEvents interactableEvents;

        public bool IsSelected { get; private set; }
        public GameObject GameObject => gameObject;

        public bool Detected { get; protected set; }

        public virtual void DetectedByVisitor(IDetectableVisitor visitor)
        {
            Detected = true;
        }

        public virtual void UndetectedByVisitor(IDetectableVisitor visitor)
        {
            Detected = false;
        }

        public virtual void OnHoverEnter(GameObject _visitor)
        {
            IsSelected = true;
            interactableEvents.onHoverEnter?.Invoke();
        }

        public virtual void OnHoverExit(GameObject _visitor)
        {
            IsSelected = false;
            interactableEvents.onHoverExit?.Invoke();
        }

        public virtual void OnInteract(GameObject _visitor)
        {
            interactableEvents.onInteract?.Invoke();
        }
    }
}


