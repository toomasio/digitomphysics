﻿using UnityEngine;
using UnityEngine.InputSystem;
using DigitomUtilities;
using DigitomPhysics;

namespace DigitomPhysics
{
    //public class UserGrab : UserInteract
    //{
    //    [SerializeField] protected InputActionReference dropInput;
    //    [SerializeField] protected Transform pickupPos;
    //    [SerializeField, ExpandableObject] protected SphereCastData dropRayData;
    //    [SerializeField] protected Transform dropRayPos;

    //    protected IPhysicsDetectable3D dropRaySystem;

    //    public bool IsGrabbing { get; private set; }

    //    protected override void OnEnable()
    //    {
    //        base.OnEnable();
    //        if (dropRayData) dropRaySystem = dropRayData.CreateSystemInstance(dropRayPos);
    //    }

    //    protected virtual void LateUpdate()
    //    {
    //        CheckGrab();
    //        DetectDrop();
    //    }

    //    protected override void EnableInput()
    //    {
    //        base.EnableInput();
    //        if (!dropInput) return;
    //        dropInput.action.performed += OnDropInput;
    //    }

    //    protected override void DisableInput()
    //    {
    //        base.DisableInput();
    //        if (!dropInput) return;
    //        dropInput.action.Disable();
    //    }

    //    protected override void OnInteract()
    //    {
    //        base.OnInteract();
    //        if (curInteractable == null) return;

    //        if (IsGrabbing)
    //        {
    //            if (!dropInput) OnDrop();
    //            else IsGrabbing = false;
    //        }
    //        else
    //            IsGrabbing = true;
    //    }

    //    protected virtual void OnDropInput(InputAction.CallbackContext ctx)
    //    {
    //        OnDrop();
    //    }

    //    protected virtual void OnDrop()
    //    {
    //        if (!IsGrabbing || curInteractable == null) return;

    //        if (dropRayData)
    //        {
    //            if (dropRaySystem.Detected)
    //                curInteractable.GameObject.transform.position = dropRaySystem.HitPosition;
    //            else
    //            {
    //                Debug.Log("Cannot drop here! No collider in layer " + LayerMask.LayerToName(dropRayData.mask) + " in range!");
    //                return;
    //            }
    //        }

    //        IsGrabbing = false;
    //    }

    //    protected override void DoHoverExit()
    //    {
    //        if (IsGrabbing) return;

    //        base.DoHoverExit();
    //    }

    //    void CheckGrab()
    //    {
    //        if (!IsGrabbing || curInteractable == null) return;

    //        Transform trans = curInteractable.GameObject.transform;
    //        trans.position = pickupPos.position;
    //        trans.rotation = pickupPos.rotation;
    //    }

    //    void DetectDrop()
    //    {
    //        if (!dropRayData || !IsGrabbing) return;

    //        dropRaySystem.Direction = Vector3.down;
    //        dropRaySystem.Tick();
    //    }
    //}
}


