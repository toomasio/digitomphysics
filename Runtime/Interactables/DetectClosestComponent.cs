﻿using UnityEngine;
using UnityEngine.InputSystem;
using DigitomUtilities;

namespace DigitomPhysics
{
    public abstract class DetectClosestComponent<TDetectData, TDetectSystem, TClosestComponent> : IDetectableVisitor
        where TDetectData : IPhysicsDetectable3DSettings
        where TDetectSystem : IPhysicsDetectable3D
        where TClosestComponent : Component, IDetectable
    {
        [field: SerializeField] public virtual Transform DetectOrigin { get; set; }

        protected abstract void OnDetectEnter();
        protected abstract void OnDetectExit();

        public virtual TDetectSystem DetectSystem { get; protected set; }
        public virtual TClosestComponent CurInteractable { get; protected set; }

        public virtual GameObject GameObject { get; protected set; }

        protected abstract System.Type[] IgnoreComponents { get; }

        public bool Enabled { get; set; }

        protected readonly bool closestHitOnly;
        protected TClosestComponent lastClosest;

        public DetectClosestComponent(GameObject owner, Transform detectOrigin, TDetectData data, bool closestHitOnly = false)
        {
            this.GameObject = owner;
            this.DetectOrigin = detectOrigin;
            this.DetectSystem = (TDetectSystem)data.GetSystem(DetectOrigin);
            this.closestHitOnly = closestHitOnly;
        }

        public virtual void Tick()
        {
            if (!Enabled) return;
            if (DetectSystem == null) return;
            DetectSystem.Tick();
            DetectClosestInteractable();
        }

        public virtual void DrawDetectable()
        {
            if (!Enabled) return;
            if (DetectSystem != null)
                DetectSystem.DrawDetectable();
        }

        //public virtual void SubscribeEvents()
        //{
        //    if (DetectSystem == null) return;
        //    DetectSystem.OnFirst += OnHoverFirst;
        //    DetectSystem.OnEmpty += OnHoverLast;
        //}

        //public virtual void UnSubscribeEvents()
        //{
        //    if (DetectSystem == null) return;
        //    DetectSystem.OnFirst -= OnHoverFirst;
        //    DetectSystem.OnEmpty -= OnHoverLast;
        //}

        void DetectClosestInteractable()
        {
            if (DetectSystem == null) return;
            //if (CurInteractable == null) return;
            //if (DetectSystem.HitInfos.Length < 2) return;

            if (closestHitOnly)
            {
                float closest = Mathf.Infinity;
                Collider closestCol = null;

                //get closest interactable
                for (int i = 0; i < DetectSystem.HitInfos.Length; i++)
                {
                    var col = DetectSystem.HitInfos[i].collider;
                    if (col == null) continue;
                    var point = col.ClosestPoint(DetectOrigin.position);
                    var dist = (point - DetectOrigin.position).magnitude;
                    if (dist < closest)
                    {
                        closestCol = col;
                        closest = dist;
                    }
                }

                if (IsHitValid(closestCol, out var compTemp))
                {
                    if (compTemp != lastClosest)
                    {
                        lastClosest = compTemp;
                        lastClosest.UndetectedByVisitor(this);
                        OnDetectExit();
                    }

                    CurInteractable = compTemp;
                    if (!CurInteractable.Detected)
                    {
                        CurInteractable.DetectedByVisitor(this);
                        OnDetectEnter();
                    }

                }
                else
                    CurInteractable = null;
            }
            else
            {
                float closest = Mathf.Infinity;
                //get closest interactable
                for (int i = 0; i < DetectSystem.HitInfos.Length; i++)
                {
                    var col = DetectSystem.HitInfos[i].collider;
                    if (!IsHitValid(col, out var compTemp)) continue;
                    var point = col.ClosestPoint(DetectOrigin.position);
                    var dist = (point - DetectOrigin.position).magnitude;
                    if (dist < closest)
                    {
                        if (CurInteractable != null && CurInteractable.Detected)
                        {
                            CurInteractable.UndetectedByVisitor(this);
                            OnDetectExit();
                        }
                        CurInteractable = compTemp;
                        if (!CurInteractable.Detected)
                        {
                            CurInteractable.DetectedByVisitor(this);
                            OnDetectEnter();
                        }
                        closest = dist;
                    }
                }
            }
        }

        private bool IsHitValid(Collider col, out TClosestComponent component)
        {
            if (col == null)
            {
                component = null;
                return false;
            }
            bool valid = true;
            if (IgnoreComponents != null)
            {
                bool ignore = false;
                for (int j = 0; j < IgnoreComponents.Length; j++)
                {
                    if (col.GetComponent(IgnoreComponents[j]) != null) { ignore = true; }
                }
                if (ignore) valid = false;
            }

            component = col.GetComponent<TClosestComponent>();
            if (component == null) { valid = false; }

            return valid;
        }

        //protected virtual void OnHoverFirst(HitInfo obj)
        //{
        //    if (CurInteractable == null)
        //    {
        //        if (IsHitValid(obj.collider, out var comp))
        //        {
        //            CurInteractable = comp;
        //            CurInteractable.OnDetectEnter(this);
        //            OnDetectEnter();
        //        }
        //    }
        //}

        //protected virtual void OnHoverLast(Collider obj)
        //{
        //    if (CurInteractable != null)
        //    {
        //        CurInteractable.OnDetectExit(this);
        //        OnDetectExit();
        //        CurInteractable = null;
        //    }
        //}

    }
}


