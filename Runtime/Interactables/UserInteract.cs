﻿using UnityEngine;
using UnityEngine.InputSystem;
using DigitomUtilities;
using DigitomPhysics;

namespace DigitomPhysics
{
    public class UserInteract : MonoBehaviour
    {
        [SerializeField] protected InputActionReference interactInput;

        protected void OnEnable()
        {
            EnableInput();
        }

        protected void OnDisable()
        {
            DisableInput();
        }

        protected virtual void EnableInput()
        {
            interactInput.action.Enable();
            interactInput.action.performed += OnInteractInput;
        }

        protected virtual void DisableInput()
        {
            interactInput.action.Disable();
        }

        protected virtual void OnInteractInput(InputAction.CallbackContext ctx)
        {
            
        }
    }
}


