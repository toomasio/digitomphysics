﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomPhysics
{
    public class OverlapBox2D : PhysicsDetectable2DSystem
    {
        protected new OverlapBox2DData data;
        public OverlapBox2D(Transform _transform, OverlapBox2DData _data) : base(_transform, _data) { data = _data; }

        Collider2D[] cols;

        private int drawTicks;
        private Vector2 lastHitPoint;

        protected override Collider2D[] DetectColliders()
        {
            cols =  Physics2D.OverlapBoxAll(pos, data.size, transform.eulerAngles.z, data.mask);

            if (cols.Length > 0)
            {
                var hit = Physics2D.BoxCast(pos, data.size, transform.eulerAngles.z, Vector2.zero, 0);
                if (hit)
                {
                    HitPosition = hit.point;
                    HitNormal = hit.normal;
                }
                else
                    lastHitPoint = default;
                
            }

            return cols;
        }

        public override void DrawDetectable()
        {
            if (!transform) return;
            if (!Application.isPlaying)
            {
                CalculatePosRot();
            }

            if (HitPosition != lastHitPoint)
            {
                drawTicks = 0;
                lastHitPoint = HitPosition;
            }
                

            if (drawTicks < 100)
            {
                Gizmos.DrawWireSphere(HitPosition, 0.2f);
                drawTicks++;
            }
            
            Gizmos.matrix = Matrix4x4.TRS(pos, transform.rotation, Vector3.one);
            Gizmos.DrawWireCube(Vector3.zero, data.size);
            
        }
    }

}

