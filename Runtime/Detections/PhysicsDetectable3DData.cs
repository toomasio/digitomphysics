﻿using DigitomUtilities;
using UnityEngine;

namespace DigitomPhysics
{
    public abstract class PhysicsDetectable3DData : ScriptableObject, ITickableComponentFactory<IPhysicsDetectable3D, Transform>
    {
        public LayerMask mask;
        public Vector3 offset;
        public Vector3 direction;
        public Space space;
        public Color emptyDebugColor = Color.white;
        public Color detectedDebugColor = Color.white;

        public abstract IPhysicsDetectable3D CreateSystemInstance(Transform _component);
    }
}