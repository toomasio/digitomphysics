﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomPhysics
{
    [Serializable]
    public class OverlapBox3DSettings : PhysicsDetectable3DSettings, IPhysicsDetectable3DSettings
    {
        public Vector3 size;
        public OverlapBox3DSettings(OverlapBox3DData data) : base(data)
        {
            this.size = data.size;
        }

        public IPhysicsDetectable3D GetSystem(Transform transform)
        {
            return new OverlapBox3D(transform, this);
        }
    }
    public class OverlapBox3D : PhysicsDetectable3DSystem
    {
        protected new OverlapBox3DSettings settings;
        public OverlapBox3D(Transform transform, OverlapBox3DData data) : base(transform, data) { settings = new OverlapBox3DSettings(data); }
        public OverlapBox3D(Transform transform, OverlapBox3DSettings data) : base(transform, data) { this.settings = data; }

        protected Collider[] collidersNonAlloc;
        protected Collider[] colliders;
        protected override HitInfo[] DetectHits()
        {
            collidersNonAlloc = new Collider[settings.maxDetections];
            Physics.OverlapBoxNonAlloc(pos, settings.size / 2, collidersNonAlloc, rot, settings.mask);

            //TODO: is this faster than garbage collection?
            int notNull = 0;
            for (int i = 0; i < collidersNonAlloc.Length; i++)
                if (collidersNonAlloc[i] != null)notNull++;
            colliders = new Collider[notNull];
            for (int i = 0; i < colliders.Length; i++)
                colliders[i] = collidersNonAlloc[i];

            var hits = new HitInfo[colliders.Length];

            for (int i = 0; i < colliders.Length; i++)
            {
                hits[i].collider = colliders[i];
            }

            return hits;
        }

        public override void DrawDetectable()
        {
            if (!transform)return;
            if (!Application.isPlaying)
            {
                CalculatePosRot();
            }
            base.DrawDetectable();
            Gizmos.matrix = Matrix4x4.TRS(pos, transform.rotation, Vector3.one);
            Gizmos.DrawWireCube(Vector3.zero, settings.size);
        }
    }

}