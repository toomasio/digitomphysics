﻿using UnityEngine;
using DigitomUtilities;

namespace DigitomPhysics
{
    public abstract class PhysicsDetectable2DData : ScriptableObject, ITickableComponentFactory<IPhysicsDetectable2D, Transform>
    {
        public LayerMask mask;
        public Vector2 offset;
        public Space space;
        
        public abstract IPhysicsDetectable2D CreateSystemInstance(Transform _component);
    }
}


