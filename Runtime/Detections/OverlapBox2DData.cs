﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomPhysics
{
    [CreateAssetMenu(menuName = "Ngn/Physics/Overlap Box 2D")]
    public class OverlapBox2DData : PhysicsDetectable2DData
    {
        public Vector3 size;

        public override IPhysicsDetectable2D CreateSystemInstance(Transform _component)
        {
            return new OverlapBox2D(_component, this);
        }
    }
}


