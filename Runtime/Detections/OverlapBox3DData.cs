﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomPhysics
{
    [CreateAssetMenu(menuName = "Ngn/Physics/Overlap Box 3D")]
    public class OverlapBox3DData : PhysicsDetectable3DData
    {
        public Vector3 size;

        public override IPhysicsDetectable3D CreateSystemInstance(Transform _component)
        {
            return new OverlapBox3D(_component, this);
        }
    }
}


