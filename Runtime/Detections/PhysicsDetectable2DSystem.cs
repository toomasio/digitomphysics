﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DigitomUtilities;

namespace DigitomPhysics
{
    public abstract class PhysicsDetectable2DSystem : IPhysicsDetectable2D
    {
        protected readonly Transform transform;
        protected readonly PhysicsDetectable2DData data;
        public PhysicsDetectable2DSystem(Transform _transform, PhysicsDetectable2DData _data)
        {
            transform = _transform;
            data = _data;
        }
        protected abstract Collider2D[] DetectColliders();
        public abstract void DrawDetectable();

        public Vector2 Direction { get; set; }
        public Collider2D[] Colliders { get; protected set; }
        public Collider2D DefaultCollider { get; protected set; }
        public bool Detected { get; protected set; }
        public Vector2 HitPosition { get; protected set; }
        public Vector2 HitNormal { get; protected set; }

        public event Action<Collider2D> OnFirst;
        public event Action<Collider2D> OnEnter;
        public event Action<Collider2D> OnExit;
        public event Action<Collider2D> OnStay;
        public event Action<Collider2D> OnEmpty;

        protected Vector2 pos;
        protected Quaternion rot;

        private List<Collider2D> curCols = new List<Collider2D>();
        private IEnumerable<Collider2D> entered;
        private IEnumerable<Collider2D> exited;

        private bool first;

        public virtual void Tick()
        {
            CalculatePosRot();

            Colliders = DetectColliders();

            Detected = Colliders.Length > 0;

            if (Detected)
            {
                if (!first)
                {
                    OnFirst?.Invoke(Colliders[0]);
                    first = true;
                }
                CheckEnter();
                CheckStay();
                CheckExit();
                DefaultCollider = Colliders[0];
            }
            else if (first)
            {
                CheckExit();
                DefaultCollider = null;
                curCols.Clear();
                first = false;
            }
        }

        protected virtual void CalculatePosRot()
        {
            if (transform == null) return;
            pos = data.space == Space.World ? (Vector2)transform.position + data.offset : (Vector2)transform.TransformPoint(data.offset);
            rot = data.space == Space.World ? Quaternion.identity : transform.rotation;
        }

        void CheckEnter()
        {
            entered = Colliders.Select(x => { return !curCols.Contains(x) ? x : null; });
            for (int i = 0; i < entered.Count(); i++)
            {
                var col = entered.ElementAt(i);
                if (col != null)
                {
                    OnEnter?.Invoke(col);
                    curCols.Add(col);
                }
            }
        }

        void CheckExit()
        {
            exited = curCols.Select(x => { return !Colliders.Contains(x) ? x : null; });
            for (int i = 0; i < exited.Count(); i++)
            {
                var col = exited.ElementAt(i);
                if (col != null)
                {
                    if (curCols.Count == 1)
                        OnEmpty?.Invoke(col);
                    OnExit?.Invoke(col);
                    curCols.Remove(col);
                }

            }
        }

        void CheckStay()
        {
            for (int i = 0; i < Colliders.Length; i++)
                OnStay?.Invoke(Colliders[i]);
        }

    }

}

