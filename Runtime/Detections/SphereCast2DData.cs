﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomPhysics
{
    [CreateAssetMenu(menuName = "Ngn/Physics/Sphere Cast 2D")]
    public class SphereCast2DData : PhysicsDetectable2DData
    {
        public float radius;
        public float distance;

        public override IPhysicsDetectable2D CreateSystemInstance(Transform _component)
        {
            return new SphereCast2D(_component, this);
        }
    }
}


