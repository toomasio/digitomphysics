﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DigitomUtilities;
using UnityEngine;

namespace DigitomPhysics
{
    [Serializable]
    public class PhysicsDetectable3DSettings
    {
        public LayerMask mask;
        public int maxDetections = 5;
        public Vector3 offset;
        public Vector3 direction;
        public Space space;
        public Color emptyDebugColor = Color.white;
        public Color detectedDebugColor = Color.white;

        public PhysicsDetectable3DSettings() { }

        public PhysicsDetectable3DSettings(PhysicsDetectable3DData data)
        {
            this.mask = data.mask;
            this.offset = data.offset;
            this.direction = data.direction;
            this.space = data.space;
            this.emptyDebugColor = data.emptyDebugColor;
            this.detectedDebugColor = data.detectedDebugColor;
        }
    }

    public class HitInfo
    {
        public Collider collider;
        public Vector3 hitPoint;
        public Vector3 hitNormal;
    }

    public abstract class PhysicsDetectable3DSystem : IPhysicsDetectable3D
    {
        protected readonly Transform transform;
        protected readonly PhysicsDetectable3DSettings settings;
        public PhysicsDetectable3DSystem(Transform transform, PhysicsDetectable3DData data)
        {
            this.transform = transform;
            this.settings = new PhysicsDetectable3DSettings(data);
            curDetectColor = this.settings.emptyDebugColor;
        }
        public PhysicsDetectable3DSystem(Transform transform, PhysicsDetectable3DSettings settings)
        {
            this.transform = transform;
            this.settings = settings;
            curDetectColor = this.settings.emptyDebugColor;
        }
        protected abstract HitInfo[] DetectHits();
        public virtual void DrawDetectable()
        {
            Gizmos.color = curDetectColor;
        }

        protected bool overridingPosition;
        protected Vector3 positionOverride;
        public Vector3 PositionOverride { get { return positionOverride; } set { positionOverride = value; overridingPosition = true; } }
        public Vector3 Offset { get; set; }
        public Vector3 Direction { get; set; }
        public HitInfo[] HitInfos { get; protected set; }
        public Collider DefaultCollider { get; protected set; }
        public bool Detected { get; protected set; }
        public Vector3 HitPosition { get; protected set; }
        public Vector3 HitNormal { get; protected set; }

        public event Action<HitInfo> OnFirst;
        public event Action<HitInfo> OnEnter;
        public event Action<Collider> OnExit;
        public event Action<HitInfo> OnStay;
        public event Action<Collider> OnEmpty;

        protected Vector3 pos;
        protected Quaternion rot;

        private List<Collider> curCols = new List<Collider>();

        protected Color curDetectColor;

        private bool first;

        public virtual void Tick()
        {
            CalculatePosRot();
            HitInfos = DetectHits();
            Detected = false;
            if (HitInfos.Length > 0)
            {
                Detected = true;
                HitPosition = HitInfos[0].hitPoint;
                HitNormal = HitInfos[0].hitNormal;
                if (!first)
                {
                    OnFirst?.Invoke(HitInfos[0]);
                    curDetectColor = settings.detectedDebugColor;
                    first = true;
                }
                CheckEnter();
                CheckStay();
                CheckExit();
                DefaultCollider = HitInfos[0].collider;
            }
            else if (first)
            {
                ClearBuffer();
                DefaultCollider = null;
                first = false;
            }
        }

        protected virtual void CalculatePosRot()
        {
            if (transform == null) return;

            if (settings.direction != Vector3.zero)
                Direction = settings.space == Space.Self ? transform.TransformDirection(settings.direction) : settings.direction;

            if (overridingPosition)
                pos = PositionOverride + settings.offset;
            else
                pos = settings.space == Space.World ? transform.position + settings.offset + Offset : transform.TransformPoint(settings.offset + Offset);
            rot = settings.space == Space.World ? Quaternion.identity : transform.rotation;
        }

        protected virtual void OnEnterBase(HitInfo hitInfo)
        {
            OnEnter?.Invoke(hitInfo);
        }

        void CheckEnter()
        {
            //loop through all colliders to find which entered
            for (int i = 0; i < HitInfos.Length; i++)
            {
                bool match = false;
                var col = HitInfos[i];
                for (int j = 0; j < curCols.Count; j++) //loop through curCols to find a match
                {
                    if (col.collider == curCols[j])
                        match = true;
                }
                if (!match) //no match? new collider entered
                {
                    OnEnterBase(col);
                    curCols.Add(col.collider);
                }
            }
        }

        void CheckExit()
        {
            //loop through current colliders
            for (int i = 0; i < curCols.Count; i++)
            {
                bool match = false;
                var col = curCols[i];
                for (int j = 0; j < HitInfos.Length; j++) //loop through Colliders to find match
                {
                    if (col == HitInfos[j].collider)
                        match = true;
                }
                if (!match) //no match? collider must have exited
                {
                    OnExit?.Invoke(col);
                    curCols.RemoveAt(i); //can only remove one per frame safely in lists..hence ClearBuffer()
                }
            }
        }

        void ClearBuffer()
        {
            //exit the rest of the buffer
            for (int i = 0; i < curCols.Count; i++)
            {
                var col = curCols[i];
                OnExit?.Invoke(col);
                if (i == curCols.Count - 1) //last one to remove? must be empty
                {
                    curDetectColor = settings.emptyDebugColor;
                    OnEmpty?.Invoke(col);
                }
            }
            curCols.Clear();
        }

        void CheckStay()
        {
            for (int i = 0; i < HitInfos.Length; i++)
                OnStay?.Invoke(HitInfos[i]);
        }

    }

}