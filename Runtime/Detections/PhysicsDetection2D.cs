﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace DigitomPhysics
{
    public class PhysicsDetection2D : MonoBehaviour
    {
        [SerializeField] private PhysicsDetectTrigger2D[] physicsTriggers = null;

        public void OnEnable()
        {
            for (int i = 0; i < physicsTriggers.Length; i++)
            {
                physicsTriggers[i].Enable();
            }
        }

        public void OnDisable()
        {
            for (int i = 0; i < physicsTriggers.Length; i++)
            {
                physicsTriggers[i].Disable();
            }
        }

        private void FixedUpdate()
        {
            for (int i = 0; i < physicsTriggers.Length; i++)
            {
                physicsTriggers[i].Tick();
            }
        }

        void OnDrawGizmosSelected()
        {
            if (physicsTriggers == null) return;
            for (int i = 0; i < physicsTriggers.Length; i++)
            {
                physicsTriggers[i].DrawDetectable();
            }
        }
    }
}


