﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace DigitomPhysics
{
    public abstract class Physics3DDetectionContainer<T> : MonoBehaviour, IPhysicsDetectable3DContainer
        where T : IPhysicsDetectable3DSettings
    {
        [SerializeField] private PhysicsDetectTrigger<T>[] physicsTriggers = null;

        public void OnEnable()
        {
            for (int i = 0; i < physicsTriggers.Length; i++)
            {
                physicsTriggers[i].Enable();
            }
        }

        public void OnDisable()
        {
            for (int i = 0; i < physicsTriggers.Length; i++)
            {
                physicsTriggers[i].Disable();
            }
        }

        private void FixedUpdate()
        {
            for (int i = 0; i < physicsTriggers.Length; i++)
            {
                physicsTriggers[i].Tick();
            }
        }

        void OnDrawGizmosSelected()
        {
            if (physicsTriggers == null)return;
            for (int i = 0; i < physicsTriggers.Length; i++)
            {
                physicsTriggers[i].DrawDetectable();
            }
        }

        public IPhysicsDetectable3D GetSystem(string _name)
        {
            var detect = physicsTriggers.FirstOrDefault(x => x.name == _name);
            if (detect != null)
                return detect.GetSystem();

            return null;
        }
    }
}