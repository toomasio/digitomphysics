﻿using DigitomEvents;
using DigitomUtilities;
using UnityEngine;

namespace DigitomPhysics
{
    [System.Serializable]
    public class PhysicsDetectTrigger<T>
        where T : IPhysicsDetectable3DSettings
    {
        public string name;
        [SerializeField] private Transform transform = null;
        [SerializeField] private T settings = default;
        [SerializeField] private UnityEventContainer onFirstEvents = null;
        [SerializeField] private UnityEventContainer onEmptyEvents = null;
        public IPhysicsDetectable3D system;

        public void Enable()
        {
            system = GetSystem();
            system.OnFirst += DoOnFirstEvents;
            system.OnEmpty += DoOnEmptyEvents;
        }

        public void Disable()
        {
            system.OnFirst -= DoOnFirstEvents;
            system.OnEmpty -= DoOnEmptyEvents;
        }

        public void Tick()
        {
            system.Tick();
        }

        void DoOnFirstEvents(HitInfo hitInfo)
        {
            if (hitInfo.collider.transform == transform)return;
            onFirstEvents.unityEvents?.Invoke();
        }

        void DoOnEmptyEvents(Collider col)
        {
            onEmptyEvents.unityEvents?.Invoke();
        }

        public void DrawDetectable()
        {
            if (settings == null) return;
            if (!Application.isPlaying)
                system = settings.GetSystem(transform);
            system.DrawDetectable();
        }

        public IPhysicsDetectable3D GetSystem()
        {
            if (system == null)
                system = settings.GetSystem(transform);
            return system; 
        }

    }
}