﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DigitomPhysics
{
    public class SphereCast2D : PhysicsDetectable2DSystem
    {
        protected new SphereCast2DData data;
        public SphereCast2D(Transform _transform, SphereCast2DData _data) : base(_transform, _data) { data = _data; }
        private int drawSteps;

        private Collider2D[] cols;
        private RaycastHit2D[] hits;
        protected override Collider2D[] DetectColliders()
        {
            if (transform == null) return null;
            Ray2D ray = new Ray2D(pos, Direction);
            hits = Physics2D.CircleCastAll(ray.origin, data.radius, ray.direction, data.distance, data.mask);
            cols = new Collider2D[hits.Length];
            for (int i = 0; i < hits.Length; i++)
                cols[i] = hits[i].collider;

            if (hits.Length > 0)
            {
                HitPosition = hits[0].point;
                HitNormal = hits[0].normal;
            }

            return cols;
            
        }
 
        public override void DrawDetectable()
        {
            if (!transform) return;
            if (!Application.isPlaying)
            {
                CalculatePosRot();
            }

            var dest = pos + (Direction * data.distance);
            var diff = (dest - (Vector2)transform.position);

            var col = Color.white;
            if (Colliders != null)
            {
                if (Colliders.Length > 0)
                    col = Color.green;
                if (hits.Length > 0)
                {
                    Gizmos.matrix = Matrix4x4.identity;
                    Gizmos.color = Color.red;
                    Gizmos.DrawWireSphere(hits[0].point, data.radius);
                }
                    
            }
            col.a = 0.3f;
            Gizmos.color = col;
            if (Direction == default) return;
            Gizmos.matrix = Matrix4x4.TRS(pos, Quaternion.LookRotation(Direction, Vector3.up), Vector3.one);
            Gizmos.DrawWireSphere(Vector3.zero, data.radius);
            Gizmos.DrawWireSphere(Vector3.forward * diff.magnitude, data.radius);
            var right = new Vector3(data.radius, 0, 0);
            var left = new Vector3(-data.radius, 0, 0);
            var up = new Vector3(0, data.radius, 0);
            var down = new Vector3(0, -data.radius, 0);
            Gizmos.DrawLine(right, right + Vector3.forward * diff.magnitude);
            Gizmos.DrawLine(left, left + Vector3.forward * diff.magnitude);
            Gizmos.DrawLine(up, up + Vector3.forward * diff.magnitude);
            Gizmos.DrawLine(down, down + Vector3.forward * diff.magnitude);

        }
    }

}

