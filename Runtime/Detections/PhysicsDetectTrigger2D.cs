﻿using UnityEngine;
using DigitomUtilities;
using UnityEngine.Events;

namespace DigitomPhysics
{
    [System.Serializable]
    public class PhysicsDetectTrigger2D
    {
        public string name;
        [SerializeField, ExpandableObject] private PhysicsDetectable2DData data = null;
        [SerializeField] private Transform transform = null;
        [SerializeField] private UnityEvent onFirst = null;
        [SerializeField] private UnityEvent onEmpty = null;
        private IPhysicsDetectable2D system;

        public void Enable()
        {
            system = data.CreateSystemInstance(transform);
            system.OnFirst += DoOnFirstEvents;
            system.OnEmpty += DoOnEmptyEvents;
        }

        public void Disable()
        {
            system.OnFirst -= DoOnFirstEvents;
            system.OnEmpty -= DoOnEmptyEvents;
        }

        public void Tick()
        {
            system.Tick();
        }

        void DoOnFirstEvents(Collider2D col)
        {
            onFirst?.Invoke();
        }

        void DoOnEmptyEvents(Collider2D col)
        {
            onEmpty?.Invoke();
        }

        public void DrawDetectable()
        {
            if (data == null) return;
            if (system == null) system = data.CreateSystemInstance(transform);
            system.DrawDetectable();
        }

    }
}


