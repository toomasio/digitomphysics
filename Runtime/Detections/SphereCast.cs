﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DigitomPhysics
{

    [Serializable]
    public class SphereCastSettings : PhysicsDetectable3DSettings, IPhysicsDetectable3DSettings
    {
        public float radius;
        public float distance;
        public bool debugHits;
        public float debugHitRadius = 0.05f;
        public float debugHitTime = 1;

        public SphereCastSettings() { }

        public SphereCastSettings(SphereCastData data) : base(data)
        {
            radius = data.radius;
            distance = data.distance;
        }

        public IPhysicsDetectable3D GetSystem(Transform transform)
        {
            return new SphereCast(transform, this);
        }
    }

    public class SphereCast : PhysicsDetectable3DSystem
    {
        protected new SphereCastSettings settings;
        public SphereCastSettings Settings { get { return settings; } set { settings = value; } }
        public SphereCast(Transform _transform, SphereCastData _data) : base(_transform, _data) { settings = new SphereCastSettings(_data); }
        public SphereCast(Transform _transform, SphereCastSettings settings) : base(_transform, settings) { this.settings = settings; }

        private HitInfo[] hitInfos;
        private RaycastHit[] hits;

        private float debugHitTimer = 0;

        protected override HitInfo[] DetectHits()
        {
            Ray ray = new Ray(pos, Direction);
            hits = new RaycastHit[settings.maxDetections];
            Physics.SphereCastNonAlloc(ray, settings.radius, hits, settings.distance, settings.mask);
            int notNull = 0;
            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i].collider != null)
                    notNull++;
            }
            hitInfos = new HitInfo[notNull];
            for (int i = 0; i < notNull; i++)
            {
                hitInfos[i] = new HitInfo
                {
                    collider = hits[i].collider,
                    hitPoint = hits[i].point,
                    hitNormal = hits[i].normal
                };
            }

            return hitInfos;
        }

        public void Sync(SphereCastSettings data)
        {
            settings = data;
        }

        public override void DrawDetectable()
        {
            if (!transform) return;
            if (!Application.isPlaying)
            {
                CalculatePosRot();
            }

            var dest = pos + (Direction * settings.distance);

            var col = settings.emptyDebugColor;
            if (HitInfos != null)
            {
                if (HitInfos.Length > 0)
                    col = settings.detectedDebugColor;
                if (hits.Length > 0)
                    DebugHits();
                else
                    ResetDebugHit();
            }
            col.a = 0.3f;
            Gizmos.color = col;
            if (Direction == Vector3.zero) return;
            Gizmos.matrix = Matrix4x4.TRS(pos, Quaternion.LookRotation(Direction, Vector3.up), Vector3.one);
            Gizmos.DrawWireSphere(Vector3.zero, settings.radius);
            Gizmos.DrawWireSphere(Vector3.forward * settings.distance, settings.radius);
            var right = new Vector3(settings.radius, 0, 0);
            var left = new Vector3(-settings.radius, 0, 0);
            var up = new Vector3(0, settings.radius, 0);
            var down = new Vector3(0, -settings.radius, 0);
            Gizmos.DrawLine(right, right + Vector3.forward * settings.distance);
            Gizmos.DrawLine(left, left + Vector3.forward * settings.distance);
            Gizmos.DrawLine(up, up + Vector3.forward * settings.distance);
            Gizmos.DrawLine(down, down + Vector3.forward * settings.distance);

        }

        protected override void OnEnterBase(HitInfo hitInfo)
        {
            base.OnEnterBase(hitInfo);
            ResetDebugHit();
        }

        void DebugHits()
        {
            if (!settings.debugHits) return;

            debugHitTimer += Time.deltaTime;
            if (debugHitTimer < settings.debugHitTime)
            {
                for (int i = 0; i < hits.Length; i++)
                {
                    Gizmos.matrix = Matrix4x4.identity;
                    Gizmos.color = Color.red;
                    Gizmos.DrawWireSphere(hits[i].point, settings.debugHitRadius);
                }

            }

        }

        void ResetDebugHit()
        {
            debugHitTimer = 0;
        }
    }

}