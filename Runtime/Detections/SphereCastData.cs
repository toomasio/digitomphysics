﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomPhysics
{
    [CreateAssetMenu(menuName = "Ngn/Physics/Sphere Cast")]
    public class SphereCastData : PhysicsDetectable3DData
    {
        public float radius;
        public float distance;

        public override IPhysicsDetectable3D CreateSystemInstance(Transform _component)
        {
            return new SphereCast(_component, this);
        }
    }
}


