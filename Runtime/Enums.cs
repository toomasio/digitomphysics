using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomPhysics
{
    public enum AimType
    {
        TransformForward,
        Manual,
        External,
    }
}