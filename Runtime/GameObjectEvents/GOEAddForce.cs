﻿using System.Collections;
using System.Collections.Generic;
using DigitomEvents;
using UnityEngine;

namespace DigitomPhysics
{
    public class GOEAddForce : GameObjectEvent
    {
        [SerializeField] private AddForce3DSettings addForce3DSettings;
        private AddForce3D addForce;

        protected override void DoAffectedObjectEvent(GameObject affectedObject)
        {
            var rb = affectedObject.GetComponent<Rigidbody>();
            if (rb)
            {
                addForce = new AddForce3D(rb, sender.transform, addForce3DSettings);
                addForce.Tick();
            }
            else
            {
                Debug.Log(receiver.name + " does not have a " + typeof(Rigidbody) + " component!");
            }
        }
    }
}