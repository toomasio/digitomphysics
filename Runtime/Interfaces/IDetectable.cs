﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomPhysics
{
    public interface IDetectable
    {
        void DetectedByVisitor(IDetectableVisitor visitor);
        void UndetectedByVisitor(IDetectableVisitor visitor);
        bool Detected { get; }
        GameObject GameObject { get; }
    }
}

