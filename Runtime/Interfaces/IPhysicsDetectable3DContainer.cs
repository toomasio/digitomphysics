﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomPhysics
{
    public interface IPhysicsDetectable3DContainer
    {
        IPhysicsDetectable3D GetSystem(string name);
    }
}