﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomPhysics
{
    public interface IPhysicsDetectable3DSettings
    {
        IPhysicsDetectable3D GetSystem(Transform transform);
    }
}