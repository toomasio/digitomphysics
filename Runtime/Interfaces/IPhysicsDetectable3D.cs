﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomPhysics
{
    public interface IPhysicsDetectable3D : ITickable
    {
        Vector3 PositionOverride { get; set; }
        Vector3 Offset { get; set; }
        Vector3 Direction { get; set; }
        HitInfo[] HitInfos { get; }
        Collider DefaultCollider { get; }
        Vector3 HitPosition { get; }
        Vector3 HitNormal { get; }
        bool Detected { get; }
        event System.Action<HitInfo> OnFirst;
        event System.Action<HitInfo> OnEnter;
        event System.Action<Collider> OnExit;
        event System.Action<HitInfo> OnStay;
        event System.Action<Collider> OnEmpty;
        void DrawDetectable();
    }
}