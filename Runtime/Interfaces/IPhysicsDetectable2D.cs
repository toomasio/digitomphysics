﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitomUtilities;

namespace DigitomPhysics
{
    public interface IPhysicsDetectable2D : ITickable
    {
        Vector2 Direction { get; set; }
        Collider2D[] Colliders { get; }
        Collider2D DefaultCollider { get; }
        Vector2 HitPosition { get; }
        Vector2 HitNormal { get; }
        bool Detected { get; }
        event System.Action<Collider2D> OnFirst;
        event System.Action<Collider2D> OnEnter;
        event System.Action<Collider2D> OnExit;
        event System.Action<Collider2D> OnStay;
        event System.Action<Collider2D> OnEmpty;
        void DrawDetectable();
    }
}
