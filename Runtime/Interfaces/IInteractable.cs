﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomPhysics
{
    public interface IInteractable
    {
        void OnInteract(GameObject _gameobject);
        void OnHoverEnter(GameObject _gameobject);
        void OnHoverExit(GameObject _gameobject);
        GameObject GameObject { get; }
    }
}

