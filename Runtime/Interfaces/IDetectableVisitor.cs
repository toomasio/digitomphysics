﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomPhysics
{
    public interface IDetectableVisitor
    {
        GameObject GameObject { get; }
    }
}

